Now that the interface files of the module and its partitions have been
covered (and in practice: compiled), the source files of the module and its
partitions can be compiled. To compile those files a build-utility can again
be used.

Source files of partitions must first declare their module, followed by
importing their partition. E.g., for the tt(Utility) partition this boils down
to: 
    verb(    module Math;
    import :Utility;)
        Following these lines other specifications may be provided, like
importing additional components (e.g., `tt(import <iostream>;)') or declaring
namespaces.
    hi(partition: no #include) Since modules shouldn't use tt(#include)
directives, partitions don't need tailored (internal) header files. Instead,
as covered before, when implementing a partition component using 
    em(frame file)em(s) are advised: when defining a new source file start by
copying tt(frame) to the new source file, followed by completing its
implementation.  For tt(Math:Utility) the following frame file is used:
    verbinsert(-as4 examples/partition/utility/framex)

Beyond the required declaration and imports components of partitions (and
modules) are implemented as usual. E.g., here is the implementation of
tt(Math:Utility's class Utility's) constructor:
    verbinsert(-as4 examples/partition/utility/utility1.cc)

The tt(Math:Add) partition's remaining source files also start from a
tt(frame) file:
    verbinsert(-as4 examples/partition/add/framex)

Since tt(frame) satisfies all requirements tt(Add's) members can now
straightforwardly be implemented. Here's tt(Add's) constructor:
    verbinsert(-as4 examples/partition/add/add1.cc)
  and its tt(sum) member, after calling tt(Uility::inc), simply returns the
sum of its two parameter values:
    verbinsert(-as4 examples/partition/add/sum.cc)

The tt(Math) module defines its own a tt(frame) file, which is used to start
the definition of the members of its tt(Math) module:
    verbinsert(-as4 examples/partition/math/framex)

The tt(Math) module's tt(class Math) defines a constructor and two members,
which can be defined as usual. Its constructor:
    verbinsert(-as4 examples/partition/math/math1.cc)
  Its tt(count) member:
    verbinsert(-as4 examples/partition/math/count.cc)
  and its tt(add) member:
    verbinsert(-as4 examples/partition/math/add.cc)


