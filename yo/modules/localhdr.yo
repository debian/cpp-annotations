Although local header files can also be module-compiled doing so should be
avoided as doing so is probably a left-over from the traditional program
development era.

In the introductory paragraphs of this chapter it was already mentioned that,
when using modules, using a em(frame) file is preferred over using (internal)
local header files: source files importing modules or defining module
components are all plain source files, and therefore, since the frame file
imports all required module components it's easy to define another component
or another source file using modules. It's of course possible that at some
point, during the implementation of such sources additional imports are
required. In those cases simply add the additional imports to the frame
file. It doesn't invalidate the already defined source files and simplifies
defining new source files.

But, for the sake of completeness, local header files em(can) also be
(module-)compiled. However, note that once a header file is compiled tt(using
namespace) declarations specified in header files are lost when the header
file is either included or imported. Compiled local header files are
stored in the tt(gcm.cache/,/) (note: a comma) sub-directory.  They're
automatically used when either their header files are included (e.g.,
tt(#include "header.h")) or imported (using tt(import "header.h";)).

