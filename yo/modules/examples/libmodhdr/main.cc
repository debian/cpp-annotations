// 1. libheaders contains iostream1 and iostream1.gcm
// 2. in gcm.cache:     ln -s /home .
// 3. compile main.cc using
//      gm -c -isystem  /home/frank/libmodhdr/libheaders main.cc
// or
//      gm -c -isystem  ~/libmodhdr/libheaders main.cc
//
// This also works if ~/libmodhdr is in fact a link to some other location.
// E.g., after:
// ln -s /home/frank/src/annotations/yo/modules/examples/libmodhdr/libheaders .
//      gm -c -isystem  ~/libheaders main.cc
// so in gcm.cache: a link to /home, in /home/frank a link to the actual
// location of libheaders, where the library's headers and module-compiled
// headers  are located.

// Not OK: can't be relative. To use this a link to libheaders from
// gcm.cache/, is needed:
//      in gcm.cache:
//          ln -s ../../libheaders .
//  then:
//      gm -c -isystem  libheaders main.cc


// -idirafter <dir>          Add <dir> to the end of the system include path.
// -isystem <dir>            Add <dir> to the start of the system include path.
// -iwithprefix <dir>        Add <dir> to the end of the system include path.
// -iwithprefixbefore <dir>  Add <dir> to the end of the main include path.

// -imacros <file>           Accept definition of macros in <file>.
// -imultilib <dir>          Set <dir> to be the multilib include subdirectory.
// -include <file>           Include the contents of <file> before other files.
// -iprefix <path>           Specify <path> as a prefix for next two options.
// -iquote <dir>             Add <dir> to the end of the quote include path.
// -isysroot <dir>           Set <dir> to be the system root directory.

//#include <iostream1>
import <iostream1>;

int main()
{}
