export module Math:Utility;

export import <cstddef>;

export
{
    class Utility
    {
        size_t d_count;

        public:
            Utility();

            void inc();
            size_t count() const;
    };
}

inline void Utility::inc()
{
    ++d_count;
}

inline size_t Utility::count() const
{
    return d_count;
}
