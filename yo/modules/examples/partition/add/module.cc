export module Math:Add;

export import :Utility;
export import <cstddef>;

export
{
    class Add
    {
        Utility &d_utility;

        public:
            Add(Utility &utility);
            size_t sum(size_t lhs, size_t rhs);
    };
}
