module Math;
import :Add;

size_t Add::sum(size_t lhs, size_t rhs)
{
    d_utility.inc();
    return lhs + rhs;
}
