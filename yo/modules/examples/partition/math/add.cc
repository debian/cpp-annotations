import Math;

size_t Math::add(size_t lhs, size_t rhs)
{
    return d_add.sum(lhs, rhs);
}
