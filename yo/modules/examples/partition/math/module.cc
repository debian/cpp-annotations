export module Math;

export import <cstddef>;

export import :Utility;
export import :Add;

export
{
    class Math
    {
        Utility d_util;
        Add d_add;

        public:
            Math();
            size_t count() const;
            size_t add(size_t lhs, size_t rhs);
    };
}
