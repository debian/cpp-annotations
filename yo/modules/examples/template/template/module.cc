export module Template;

export
{
    template <typename Type>
    Type add(Type const &t1, Type const &t2)
    {
        return t1 + t2;
    }
}
