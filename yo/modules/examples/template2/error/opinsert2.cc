module Error;
using namespace std;

Error &operator<<(Error &error, ios_base &(*func)(ios_base &))
{
    cerr << func;
    return error;
}
