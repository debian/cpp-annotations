module Error;
using namespace std;

Error &operator<<(Error &error, ostream &(*func)(ostream &))
{
    if (ostream &(*endMsg)(ostream &) = endl; func == endMsg)
        ++error.d_count;

    cerr << func;
    return error;
}
