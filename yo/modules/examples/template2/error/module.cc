export module Error;

export import <iostream>;

export
{
    class Error
    {
        template <typename Type>
        friend Error &operator<<(Error &error, Type const &type);

        friend Error &operator<<(Error &error,                      // 2
                                 std::ios_base &(*func)(std::ios_base &));

        friend Error &operator<<(Error &error,                      // 3
                                 std::ostream &(*func)(std::ostream &));

        size_t d_count;

        public:
            Error();
            size_t count() const;
    };
}

template <typename Type>
Error &operator<<(Error &error, Type const &type)
{
    std::cerr << type;
    return error;
}

inline Error::Error()
:
    d_count(0)
{}

inline size_t Error::count() const
{
    return d_count;
}
