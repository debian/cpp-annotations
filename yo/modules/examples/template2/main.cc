import Error;
using namespace std;

int main()
{
    Error error;

    error << "hello " << 12 << '\n';
    cout << "Number of errors: " << error.count() << '\n';

    error << "after inserting std::endl: " << endl;
    cout << "Number of errors: " << error.count() << '\n';
}
