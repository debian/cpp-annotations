export module Second;

export import Basic;
export import <iosfwd>;

export
{
    class Second
    {
        public:
            static std::string hello();
    };
}
