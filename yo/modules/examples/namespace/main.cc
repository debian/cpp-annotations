import NS;
import <iostream>;

int main()
{
    ++FBB::g_count;

    std::cout << FBB::g_count << '\n';
}

// alternatively:
//
// import NS;
// import <iostream>;
//
// using namespace std;
// using namespace FBB;
//
// int main()
// {
//     ++g_count;
//     cout << g_count << '\n';
// }
