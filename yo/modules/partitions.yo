COMMENT(note: name.sub is a module name, not a partition)

In the introduction section of this chapter it was stated that modules offer
new, conceptual features: modules offer program sections which are completely
separated from the rest of the program, but modules themselves can define
subsections (called hi(partition)em(partitions)) which may define data and
types (like classes) which are only accessible to the module and its
partitions. 

If such partitions define classes then the access rights of the members of
those classes are not altered: the public members of those classes are
accessible to the members of the module and its partition, their protected
members are available to classes derived from those classes, defined in the
module or its partitions, while their private members are not available
outside of the class.

figure(modules/partition)
        (Partitions of the `Math' module)
        (MathPart)

Figure ref(MathPart) illustrates a simple module (tt(Math)) having two
partitions. It shows the relationships between the module and its partitions
in the same way as the relationships between classes are commonly displayed:
the most basic (least dependent) partition is on top, the partition depending
on the topmost partition is in the middle, and the module depending on both
partitions is at the bottom. The tt(Math) module defines a class tt(Math),
defining a member returning the sum of two tt(size_t) values and a member
tt(count) returning the number of performed additions. It's a very simple
design, illustrating the way partitions are designed. The tt(class Add) object
performs the addition, and the tt(class Utility) object keeps track of the
number of additions that were performed.  Both tt(Add)
and tt(Utility) are defined as classes in their own partitions:
tt(Math:Utility) and tt(Math:Add).

hi(partition: syntax) Partitions are not em(class) members: partitions are
specified using only a em(single) colon instead of two, as used then defining
cass member functions.

The tt(Math) module and its tt(Math) class are exported, so the em(class)
tt(Math) can be used by other source files after importing the em(module)
tt(Math):
    verb(    import Math;)
    On the other hand, tt(Math:Add) and tt(Math:Utility) are partitions, and
components of partitions can+em(not) be imported by sources not belonging to
the tt(Math) module or its partitions. Partitions, therefore, allow the
implementation of partially private components: not accessible outside of the
module, but accessible all over the module itself.

The tt(Math) module itself exports all its components, including its
partitions, since they're all used by the exported tt(Math) class.  When
importing partitions their (tt(Math)) module name isn't specified: since
partitions can only be defined within the context of a module the module name
is implied, and partitions are imported without mentioning their module names:
    verb(    export import :Utility;)

Other than importing its partitions the tt(Math) module's interface contains
no other specific details, but it `exports imports' the tt(<cstddef>) module
header file since the tt(Math) class refers to the tt(size_t) type. Here is
its tt(module.cc) file:
    verbinsert(-as4 examples/partition/math/module.cc)

hi(partition: construction) What em(is) important is that what's imported by a
module must exist before the module's tt(module.cc) file can be
compiled. So the partitions tt(Utility) and tt(Add) (specifically: their
tt(gcm.cache/*.gcm) files) must be available before tt(Math's module.cc)
file can be compiled. Once the required tt(*.gcm) files are available the
remaining source files of partitions and modules can be compiled. In practice,
a em(modmapper) program (cf. section ref(MODMAPPER)) is used to prepare the
required tt(gcm.cache) files, whereafter the remaining source files can
be compiled as usual.

Since (cf. figure ref(MathPart)) the tt(Utility) partition is the most basic,
it is covered next.


