Sometimes we want to move elements from one container to another one. Instead
of retrieving elements in sequence and then moving them into the destination
container (e.g., when extracting words from streams) or explicitly calling
tt(std::move) on the source container's elements it's also possible to use
ti(make_move_iterator)(std::make_move_iterator(iterator iter)). The function's
argument is an iterator referring to a movable element, and the range of
elements to move from one container to another can be specified by calling
two tt(make_move_iterators). One receives the source's begin iterator, and
other one the source's end iterator. The example illustrates how words
can be moved into a tt(std::vector<std::string>):
        verbinclude(-as4 examples/moveiter.cc)
    Note that the elements in the source container have merely moved into the
destination container: the elements in the source container are empty after
moving. When moving other types of objects the results may be different,
although in all cases the source objects should keep their valid states.
