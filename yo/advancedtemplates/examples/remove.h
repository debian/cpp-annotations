template <typename ...Types>
struct Remove;

template<typename Rm>
struct Remove<TypeList<Rm>>                 // empty list
{
    using Removed = TypeList<>;
};

template<typename Rm, typename ...Tail>     // remove Rm fm the head,
struct Remove<TypeList<Rm, Rm, Tail...>>
{
    using Removed = typename Remove<TypeList<Rm, Tail...>>::Removed;
};

                                            // keep First, rm Rm from Tail
                                            // concatenate the two
template<typename Rm, typename First, typename ...Tail>
struct Remove<TypeList<Rm, First, Tail...>>
{
    using Removed = TypeList<First,
        typename Remove<TypeList<Rm, Tail...>>::Removed>;
};
