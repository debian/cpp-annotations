#ifndef INCLUDED_APPEND_H_
#define INCLUDED_APPEND_H_

#include "typelist.h"

//STRUCTS
    template <typename ...Types>
    struct Append;

    template <typename ...Types>
    struct Prefix;
//=

//ADDTYPE
    template <typename NewType, typename ...Types>
    struct Append<TypeList<Types...>, NewType>
    {
        using List = TypeList<Types..., NewType>;
    };

    template <typename NewType, typename ...Types>
    struct Prefix<NewType, TypeList<Types...>>
    {
        using List = TypeList<NewType, Types...>;
    };
//=

#endif
