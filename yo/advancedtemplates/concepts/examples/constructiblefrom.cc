#include <type_traits>
#include <string>

//constructible
template <typename Class, typename ...Params>
concept Constructible = std::is_constructible<Class, Params ...>::value;

template <typename Class, typename ...Params>
    requires Constructible<Class, Params ...>
void fun(Class &&type, Params &&...params)
{}
//=

int main()
{
    fun(std::string{}, "hello");
}
