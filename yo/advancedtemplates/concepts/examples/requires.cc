#include <iostream>

template <typename Type>
concept Addable =
    requires(Type lh, Type rh)
    {
        lh + rh;
    };

template <typename Type>
concept Subtractable =
    true;

//requires
template<Addable Type>
Type add2(Type const &x, Type const &y)
{
    return x + y;
}

template<typename Type>
    requires Addable<Type>
Type add(Type const &x, Type const &y)
{
    return x + y;
}
//=

//multiple
template<typename Type>
    requires
        Addable<Type> and Subtractable<Type>
Type addsub(Type const &x, Type const &y, Type const &z)
{
    return x + y - z;
}
//=

//basicmath
template <typename Type>
concept BasicMath =
    Addable<Type> and Subtractable<Type> and
    requires(Type x, Type y)
    {
        x * y;
    };
//=

//variadic
template <typename ...Types>
concept Variadic =
    requires(Types ...arguments)
    {
        true;
    };

//=


template<Addable Type>
    requires Subtractable<Type>
Type addsub2(Type const &x, Type const &y, Type const &z)
{
    return x + y - z;
}


using namespace std;

int main()
{
    add("one"s, "two"s);
    add2("one"s, "two"s);
}
