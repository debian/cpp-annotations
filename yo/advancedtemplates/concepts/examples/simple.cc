#include <string>
#include <map>
#include <iostream>

//addable
template <typename Type>
concept Addable =
    requires(Type par)
    {
        ++par;          // `Type' variables must support
        par++;          // pre-/postfix increment
    }
    and requires(Type lhs, Type rhs)    // and also addition
    {
        lhs + rhs;
    };
//=

//        typename Type::value_type;
//index
template <typename Type>
concept HasIndex =
    requires(Type tp)
    {
        tp[0];
    };

template <HasIndex Type>
auto idx(Type const &obj, size_t idx)
{
    return obj[idx];
}
//=

template <typename Type>
concept HasStrIndex =
    requires(Type tp)
    {
        tp[std::string{}];
    };

template <HasStrIndex Type>
auto value(Type &obj, std::string const &key)
{
    return obj[key];
}

using namespace std;

int main()
{
//    idx(1, 2);
    string str;
    cout << idx(str, 1) << endl;

    map<string, double> msd;
    value(msd, "hi");
}
