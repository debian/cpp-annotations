hi(jthread) In addition to tt(std::thread) the class tt(std::jthread) can be
used. 

Before using tt(jthread) objects the tthi(thread) header file must be
included.

Objects of the class tt(jthread) act like tt(thread) objects, but a
tt(jthread) thread automatically joins the thread that activated
tt(jthread). Moreover, in some situations tt(jthread) threads can directly 
be ended.

Once a tt(jthread) object receiving a function defining the thread's actions
has been constructed that function immediately starts as a separate thread.
If that function ends by returning a value then that value is ignored. If the
function throws an exception the program ends by calling
tt(std::terminate). Alternatively, if the function should communicate a return
value or an exception to, e.g., the function starting the tt(jthread) a
tt(std::promise) (cf. section (ref(PROMISE))) can be used or it can modify
variables which are shared with other threads (see also sections ref(MUTEX)
and ref(ATOMIC)).

The class tt(jthread) offers these constructors:
    itemization(
    ittq(jthread() noexcept)
       (The default constructor creates a tt(jthread) object that doesn't
start a thread. It could be used as a data member of a class, allowing class
objects to start the tt(jthread) at some later point in time;)

    ittq(explicit jthread(Function &&function, Args &&...args))
       (This constructor (which is a em(member template), cf. section
ref(MEMTEMP)) expects a function (or functor) as its first argument, starting
the thread defined by tt(function). The function receives as its first
argument the return value of tt(jthread's) member tt(get_stop_token) (see
below), followed by the tt(args) parameters (if present). If tt(function's)
first argument is not a tt(std::stop_token) then tt(function), merely
receiving the tt(args) parameter values as its arguments. Arguments are passed
to tt(function) with their proper types and values (see the example shown
below at the description of the tt(jthread) member tt(request_stop);))

    it() The class tt(jthread) supports move construction and move assignment,
but does not offer copy construction and copy assignment.
    )

The following members are available and operate like the identically named
tt(std::thread) members. Refer to section ref(THREAD) for their descriptions:
    itemization(
    itht(detach)(void detach();)
    itht(get_id)(id get_id() const noexcept;)
    itht(hardware_concurrency)
        (unsigned thread::hardware_concurrency() noexecpt)
    itht(join)(void join();)
    itht(joinable)(bool joinable() const noexcept;)
    itht(native_handle)(native_handle_type  native_handle();)
    itht(swap)(void swap(thread &other) noexcept.)
    )


The following members are specific to tt(jthread), allowing other threads to
end the thread started by tt(jthread):

    itemization(
    ithtq(get_stop_source)(std::stop_source get_stop_source() noexcept)
       (returns the hi(stop_token) tt(jthread's std::stop_source).)

    ithtq(get_stop_token)(std::get_stop_token get_stop_token() const noexcept)
       (returns the hi(stop_token) tt(jthread's std::stop_token).)

    ithtq(request_stop)(bool request_stop() noexcept)
       (attempts to end the thread started by the tt(jthread) object. The
        function operates atomically: it can be called from multiple threads
        without causing race conditions. It returns tt(true) if the stop
        request was successfully issued. It returns tt(false) if a stop
        request has already been issued, which may also happen if
        tt(request_stop) was issued by different threads, and another thread
        is still in the process of ending tt(jthread's) thread.nl() When
        issuing tt(request_stop) then tt(std::stop_callback) functions (see
        the next section) that were registered for the thread's stop state are
        synchroneously called. If those callback functions throw exceptions
        then tt(std::terminate) is called. Also, any waiting condition
        variables that are associated with the tt(jthread's) stop state end
        their waiting states.)
    )

Here is a short program illustrating tt(request_stop):
        verbinsert(-ans4 examples/stoptoken.cc)
    itemization(
    it() at line 17 the tt(jthread) thread starts, receiving function tt(fun)
        as its argument;
    it() as tt(fun) defines a tt(std::stop_token) parameter, tt(jthread) will
        start that function. It performs (line 8) a tt(while) loop that
        continues until tt(stop's stop_requested) returns tt(true). The loop
        itself shows a brief output line (line 10) followed by a one-second
        sleep (line 11);
    it() the tt(main) function, having started the thread, sleeps for three
        seconds (line 19), and then (line 21) issues a stop-request, 
        ending the thread.
    )

When running the program three lines containing tt(next) are displayed.

