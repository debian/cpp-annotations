In bf(C++) the well-known ti(sizeof) operator can be applied to data
members of classes without the need to specify an object as well. Consider:
        verb(    class Data
    {
        std::string d_name;
        ...
    };)

To obtain the size of tt(Data)'s tt(d_name) member the following
expression can be used:
        verb(    sizeof(Data::d_name);)

Note, however, that the compiler observes data protection here as well. It's
only possible to use tt(sizeof(Data::d_name)) when tt(d_name) is visible,
i.e., it can be used by tt(Data)'s member functions and friends.
