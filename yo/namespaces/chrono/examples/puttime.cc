#include <iostream>
#include <chrono>

using namespace std;
using namespace chrono;

//demo
int main()
{
    auto now = system_clock::now();         // get a time_point
    cout << now << '\n';                    // now's UTC time
                                            // convert to a time_t
    time_t timeT = system_clock::to_time_t(now);
    tm *local = localtime(&timeT);          // get a tm *
    cout << put_time(local, "%c") << '\n';  // show the time
}
//=
