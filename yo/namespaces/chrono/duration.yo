Amounts of time are specified by objects of the class
    hi(duration)tt(std::chrono::duration).

Before using the class tt(duration) the tthi(chrono) header file must be
included.

The class tt(duration) has two template arguments. A numeric type (tt(int64_t)
is normally used) defining the type holding the duration's amount of time, and
a time-resolution tt(ratio) (called its em(resolution)).

Often the following predefined tt(duration) types are used:
    center(
    tbl(lll)(\
    tline()()\
    tr(tc(tt(predefined:))tc(nbsp())tc(tt(duration type)))\
    tline()()\
    tr(tc(tt(nanoseconds))tc(nbsp())tc(tt(duration<int64_t, nano>)))\
    tr(tc(tt(microseconds))tc(nbsp())tc(tt(duration<int64_t, micro>)))\
    tr(tc(tt(milliseconds))tc(nbsp())tc(tt(duration<int64_t, milli>)))\
    tr(tc(tt(seconds))tc(nbsp())tc(tt(duration<int64_t>)))\
    tr(tc(tt(minutes))tc(nbsp())tc(tt(duration<int64_t, ratio<60>>)))\
    tr(tc(tt(hours))tc(nbsp())tc(tt(duration<int64_t, ratio<3600>>)))\
    tline()()\
    ))

E.g., to define a duration of 30 minutes use tt(minutes halfHour{ 30 }).

The suffixes tt(h, min, s, ms, us, ns) are available for integral values,
creating the corresponding tt(duration) times. E.g., tt(minutes min =
1h) stores 60 minutes in tt(min).

bf(Sub-types):

    itemization(
    itt(rep): the duration's numeric type (commonly tt(int64_t));
    itt(period): the tt(ratio) type (like tt(kilo)). E.g.,
        tt(minutes::period::num) equals 60.
    )

COMMENT(all constexpr, the constructor: explicit)

bf(Constructors):

    itemization(
    itt(duration()):nl()
        the default constructor initializes the object to 0 units;
    itt(duration(Type const &value)):nl()
        the object is initialized to tt(value) time units (tt(Type) refers to
        the duration's numeric type. E.g., when defining 
        tt(minutes halfHour{ 30 }) the argument 30 is stored inside its
        tt(int64_t) data member). 
    )
    Copy- and move-constructors (cf. chapter ref(MEMORY)) are available.

bf(Operators):

    Duration types support assignment, negation (e.g., tt(halfHour =
-halfHour)), addition and subtraction of duration values, and also
multiplication, division and modulo computations by numeric factors. Compound
assignment operators are also available. E.g.,
 verb(    minutes time = 2 * halfHour;    // time: 60 minutes
    time += minutes{ 30 };          // time: 90 minutes )

bf(Members):

    itemization(
    itt(Type count() const) returns the value stored inside the tt(duration)
        object. E.g., tt(minutes{ 30 }.count()) returns 30.  The following
        members are static members;

    itt(duration::zero()) returns an (immutable) duration object whose
        tt(count) member returns 0. E.g., tt(seconds::zero().count()) returns
        tt(0s);

    itt(duration::min()) returns an (immutable) duration object whose
        tt(count) member returns the lowest value of its tt(rep) type (i.e.,
        tt(std::numeric_limits<duration::rep>::min()) (cf. section
        ref(NUMLIM)));
    
    itt(duration::max())  returns an (immutable) duration object whose
        tt(count) member returns the maximum value of its tt(rep) type.
    )

bf(Conversions):

The precision of left-hand side arguments of assignment operators must be
greater than or equal to the precision of the right-hand side arguments
(the left-hand argument may not lose precision). When using binary
arithmetic operators the resulting duration type has a precision equal to the
finer of the two precisions. E.g.,
 verb(    cout << (1min + 1h).count() << '\n';  // shows: 61
    hours hr{ 1 };
    halfHour += hr;                 // OK
    // hr += halfHours;             // won't compile )

Durations can be converted by the hi(duration_cast)
tt(std::chrono::duration_cast<destination>(source)) function template, where
tt(destination) is the destination's tt(duration) type and tt(source) is an
available tt(destination) object, but tt(duration_cast) truncates the
destination value when tt(destination's) precision is less than tt(source's)
precision. E.g., tt(duration_cast<minutes>(seconds{ 90 })) returns 
tt(minutes{ 1 }).

When the left-hand side argument's precision em(is) less than the right-hand
side's precision some decision must be made about how to handle the loss of
precision. The following function template can be used to convert a duration
to a less precise type, returning a tt(double) value, which can be, e.g.,
truncated or rounded.
 verb(    template <typename To, typename From>
    double durationCast(From from)
    {
        return static_cast<double>(from.count()) * 
                            To::period::den * From::period::num /
                            (To::period::num * From::period::den);
    } )

returning a tt(double) value (1.5 when called as
tt(durationCast<minutes>(seconds{ 90 }))), leaving the decision how to use the
returned tt(double) value to the caller.
