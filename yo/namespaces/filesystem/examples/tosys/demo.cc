#include <iostream>
#include <filesystem>
#include <chrono>

#include <bobcat/exception>

using namespace std;
using namespace chrono;
using namespace filesystem;
using namespace FBB;

int main(int argc, char **argv)
try
{
    error_code ec;

    // comment out the 2nd clock_time_conversion template and you get the
    // first:

    time_t seconds = system_clock::to_time_t(
        clock_time_conversion<system_clock, system_clock>{}(
                                                    system_clock::now()
                                                           )
                            );

    auto syspoint = system_clock::now();
    auto filepoint = clock_time_conversion<file_clock, system_clock>
                                                            (syspoint);
//    cout << syspoint.count() << '\n' <<
//            filepoint.count() << '\n';
return 0;

    cout << put_time(localtime(&seconds), "%c") << '\n';

    auto sysTime = file_clock::to_sys(file_clock::now());

    time_t sysSecs = system_clock::to_time_t(sysTime);

    cout << put_time(localtime(&sysSecs), "%c") << '\n';


//    seconds = system_clock::to_time_t(
//        clock_time_conversion<system_clock, file_clock>{}(
//                                                    file_clock::now()
//                                                           )
//                            );
//
//    cout << put_time(localtime(&seconds), "%c") << '\n';

}
catch (exception const &exc)
{
    cout << exc.what() << '\n';

}
