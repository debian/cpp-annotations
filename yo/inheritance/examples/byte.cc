#include <iostream>
#include <cstdint>          // uint8_t

class Byte
{
    friend std::istream &operator>>(std::istream &out, Byte &byte);
    friend std::ostream &operator<<(std::ostream &out, Byte const &byte);

    uint8_t d_byte;

    public:
        Byte();                     // CC available by default
        Byte(uint8_t byte);

        Byte &operator=(Byte const &rhs) = default;
        Byte &operator=(uint8_t rhs);

        operator uint8_t &();
        operator uint8_t const &() const;
};

inline Byte::Byte()
:
    d_byte(0)
{}

inline Byte::Byte(uint8_t byte)
:
    d_byte(byte)
{}

inline Byte &Byte::operator=(uint8_t rhs)
{
    d_byte = rhs;
    return *this;
}

inline Byte::operator uint8_t &()
{
    return d_byte;
}

inline Byte::operator uint8_t const &() const
{
    return d_byte;
}

inline std::ostream &operator<<(std::ostream &out, Byte const &byte)
{
    return out << static_cast<uint16_t>(byte.d_byte);
}

std::istream &operator>>(std::istream &in, Byte &byte)
{
    size_t value;
    in >> value;
    byte.d_byte = value;

    return in;
}

using namespace std;

int main()
{
    Byte b1;
    Byte b2{ 12 };
    Byte b3{ b2 };

    b1 += 13;
    b1 = 65;

    b1 <<= 1;
    b1 >>= 1;
    b1 |= 1;

    uint8_t u8 = b1;

    cout << b1 << ' ' << b3 << ' ' << u8 << '\n';
}
