Once a function has been defined it's often called from other functions. If
called functions are not defined in the same source file as calling functions
the called functions must be declared, for which header files are often
used. Those called functions might throw exceptions, which might be
unacceptible to the function calling those other functions. E.g., functions
like tt(swap) and destructors may not throw exceptions. 

Functions that may not throw exceptions can be declared and defined by
specifying the ti(noexcept) keyword (see section ref(SYSTEMERROR) for examples
of function declarations specifying tt(noexcept)).

When using tt(noexept) there's a slight run-time overhead penalty because the
function needs an over-all tt(try-catch) block catching any exception that
might be thrown by its (called) code. When an exception is caught (violating
the tt(noexcept) specification) then the tt(catch) clause calls
tt(std::terminate), ending the program.

In addition to using a plain tt(noexcept), it can also be given an argument
that is evaluated compile-time (e.g., tt(void fun() noexcept(sizeof(int) ==
4))): if the evaluation returns tt(true) then the tt(noexcept) requirement is
used; if the evaluation returns tt(false), then the tt(noexcept) requirement
is ignored. Examples of this advanced use of tt(noexcept) are provided in
section ref(NOEXCEPT).


