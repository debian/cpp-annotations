hi(await_ready) Once the tt(Awaiter) object is available its member tt(bool
await_ready()) is called. If it returns tt(true) then the coroutine is not
suspended, but continues beyond its tt(co_await) statement (in which case
the tt(Awaitable) object and tt(Awaiter::await_ready) were apparently able to
avoid suspending the coroutine).

hi(await_suspend) If tt(await_ready) returns tt(false)
tt(Awaiter::await_suspend(handle)) is called. Its tt(handle) argument is the
handle (e.g., tt(d_handle)) of the current coroutine's handler object. Note
that at this point the coroutine has already been suspended, and the
coroutine's handle could even be transferred to another thread (in which case
the current thread must of course not be allowed to resume the current
coroutine). The member tt(await_suspend) may return tt(void, bool,) or some
coroutine's handle (optionally its own handle). As illustrated in figure
ref(AwaiterFig), when returning tt(void) or tt(true) the coroutine is
suspended and the coroutine's caller continues its execution beyond the
statement that activated the coroutine. If tt(false) is returned the coroutine
is not suspended, and resumes beyond the tt(co_await) statement. If a
coroutine's handle is returned (not a reference return type, but value return
type) then the coroutine whose handle is returned is resumed (assuming that
another coroutine's handle is returned than the current coroutine is
suspended, and the other coroutine (which was suspended up to now) is resumed;
in the next section this process is used to implement a finite state automaton
using coroutines)

    figure(coroutines/awaiter)
        (awaiter)
        (AwaiterFig)

hi(await_resume) If, following tt(await_suspend), the current coroutine is
again resumed, then just before that the tt(Awaiter) object calls
tt(Awaiter::await_resume()), and tt(await_resume's) return value is returned
by the tt(co_await) expression (tt(await_resume)) frequently defines a
tt(void) return type, as in
        verb(    static void Awaiter::await_resume() const
    {}
        )

In the next section a finite state automaton is implemented using
coroutines. Their handler classes are also tt(Awaiter) types, with
tt(await_ready) returning tt(false) and tt(await_resume) doing nothing. Thus
their definitions can be provided by a class tt(Awaiter) acting as base class
of the coroutines' handler classes. tt(Awaiter) only needs a simple header
file: 
        verbinsert(-s4 //class demo/fsa/awaiter/awaiter.h)
