Because coroutines are usually suspended once they have produced some
intermediate but useful result they offer an alternative to stack-based
approaches in which recursion is often used.

This section covers a coroutine that visits all elements of (nested)
directories, listing all their path-names relative to the original starting
directory. First a more traditional approach is covered, using a class having
a member that recursively visits directory elements. Thereafter a
coroutine is described performing the same job. Finally, some statistics about
execution times of both approaches are discussed.

