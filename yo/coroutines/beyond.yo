In the introductory section the tt(fiboCoro) coroutine was presented. In 
this section the tt(fiboCoro) coroutine is going to be used by the
tt(recursiveCoro) coroutine, using multiple levels of recursion.

To concentrate on the recursion process the tt(fiboCoroutine's) handler is
defined in tt(main) as a global object, so it can directly be used by every
recursive call of tt(recursiveCoro). Here is the tt(main) function, using
tt(bool Recursive::next(size_t *value)), and it also defines the global object
tt(g_fibo):
    verbinsert(-s4 //main demo/recursive2/main.cc)

    The tt(Recursive) class interface is identical to the one developed in the
previous section, except for the tt(Recursive::done) member (which is not used
anymore and was therefore removed from the interface), and changing tt(next)
member's signature as shown. It's implementation was altered accordingly:
    verbinsert(-s4 //next demo/recursive2/recursive/recursive.cc)

In fact, the only thing that has to be modified to process deeper recursion
levels is the tt(recursiveCoro) coroutine itself. Here is its modified
version:
    verbinsert(-ns4 //coro demo/recursive2/recursivecoro.cc)

This implementation strongly resembles the 1-level recursive coroutine. Now
multiple levels of recursion are allowed, and the maximum recursion level is
set at 5. The coroutine knows its own recursion level via its tt(size_t level)
parameter, and it recurses as long as tt(level) is less than 5 (line 8). At
each level two series of two fibonacci values are computed (in the
for-statements at lines 5 and 16). After the second for-statement the
coroutine ends unless it's the coroutine that's called from tt(main), in which
case tt(level) is 0. The decision to end (recursively called) coroutines is
made in line 19.

In this implementation the maximum recursion level is set to a fixed
value. It's of course also possible that the coroutine itself decides that
further recursion is pointless. Consider the situation where 
directory entries are examined, and where subdirectories are handled
recursively. The recursive directory visiting coroutine might then have an
implementation like this:
    verbinsert(-ans4 demo/recursive2/recursivecoro.1)

In this variant the (not implemented here) function tt(nextEntry) (line 5)
produces all directory entries in sequence, and if an entry represents a
directory (line 10), the same process is performed recursively (line 15),
yielding its entries to the current coroutine's caller (line 18).

