#ifndef INCLUDED_DIGIT_
#define INCLUDED_DIGIT_

#include "../../promisebase/promisebase.h"
#include "../awaitable/awaitable.h"

class Digit
{
    struct State: public PromiseBase<Digit, State>
    {};

    std::coroutine_handle<State> d_handle;

    public:
        using promise_type = State;
        using Handle = std::coroutine_handle<State>;

        explicit Digit(Handle handle);
        ~Digit();

        Handle handle() const;

        char const *name() const;
};

inline char const *Digit::name() const
{
    return "Digit";
}


inline Digit::Handle Digit::handle() const
{
    return d_handle;
}

extern Digit g_digit;

#endif
