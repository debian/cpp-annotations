#ifndef INCLUDED_AWAITER_
#define INCLUDED_AWAITER_

#include <coroutine>

//class
class Awaiter
{
    using Handle = std::coroutine_handle<>;

    Handle d_handle;

    public:
        Awaiter() = default;
        Awaiter(Handle handle);

        Handle await_suspend(Handle const &handle);

        static bool await_ready();
        static void await_resume();
};

inline Awaiter::Awaiter(Handle handle)
:
    d_handle(handle)
{}

inline  Awaiter::Handle Awaiter::await_suspend(Handle const &handle)
{
    return d_handle;
}

inline bool Awaiter::await_ready()
{
    return false;
}

inline void Awaiter::await_resume()
{}
//=

#endif
