    #include <iterator>

    struct ForwardIterator
    {
        using iterator_category = std::forward_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = int;
        using pointer           = value_type *;
        using reference         = value_type &;

        private:
            int d_value;

        public:
            ForwardIterator(int init);

// standard:
            bool operator==(ForwardIterator const &other) const;
            bool operator!=(ForwardIterator const &other) const;
            int &operator*();
            ForwardIterator &operator++();

// consider:
            int *operator->();
    };

ForwardIterator::ForwardIterator(int init)
:
    d_value(init)
{}

bool ForwardIterator::operator!=(ForwardIterator const &other) const
{
    return d_value != other.d_value;
}

bool ForwardIterator::operator==(ForwardIterator const &other) const
{
    return d_value == other.d_value;
}

ForwardIterator &ForwardIterator::operator++()
{
    return *this;
}

int &ForwardIterator::operator*()
{
    return d_value;
}
